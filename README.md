# MATLAB ExportSetup

This project contains a set of MATLAB export setup files that can be used to easily export camera-ready figures from MATLAB.

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

## Getting started

Clone the repository and copy all files inside the `src/` directory to MATLAB's `ExportSetup` directory. You can find its path by typing
```matlab
fullfile(prefdir(0), 'ExportSetup')
```

## Usage

Once you installed the plot styles (see [#Getting-started]), all you have to do is, beforing calling `exportgraphics`, applying the figure style `STYLENAME` you chose to your current figure:

```matlab
hgexport( ...
    gcf() ...
  , 'temp_dummy' ...
  , hgexport('readstyle', STYLENAME) ...
);
```

You can then export the figure via `exportgraphics(gcf(), 'filename.eps')` or similar.

## Support

Found an issue? Open a ticket.

Have a question? Send me an email `matlab (at) philipptempel (dot) me`

## Authors and acknowledgment

Philipp Tempel (Creator, Maintainer)
